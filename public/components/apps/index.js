import Drawer from './drawer.jsx'
import AppLiveView from './live.jsx'
import InstallView from './install.jsx'
import Explore from './explore.jsx'

export default { Drawer, AppLiveView, InstallView, Explore }
